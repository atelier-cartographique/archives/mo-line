var _ = require('lodash');
var http = require('http');
var sockjs = require('sockjs');
var debug = require('debug')('event-gen');
var generator = require('./src/generator');
var that = this;


// 1. Echo sockjs server
var sockjs_opts = {
    sockjs_url: 'http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js'
};


function splitMessage(msg) {
    var lines = msg.split('\n');
    return ({
        command: lines[0],
        args: lines.slice(1).reduce((memo, l) => {
            const [k, v] = l.split(':');
            memo[k] = v;
            return memo;
        }, {})
    });
}

var sockjs_server = sockjs.createServer(sockjs_opts);

sockjs_server.on('connection', function(conn) {
    var itv;
    conn.on('data', function(message) {
        debug(`<< ${message}`);
        var msg = splitMessage(message);
        if(msg.command === 'CONNECT') {
            conn.write(`CONNECTED\nversion:1.0\n\n\0`);
        }
        if(msg.command === 'SUBSCRIBE') {
            var subId = msg.args.id;
            itv = setInterval(function(){
                generator().forEach(function(g) {
                    if ('STOP' === g) {
                        clearInterval(itv);
                    }
                    else {
                        g.parameters.ts = Date.now();
                        var msg = JSON.stringify(g);
                        // debug(msg);
                        conn.write(`MESSAGE\nsubscription:${subId}\ndestination:/q/events\ncontent-type:application/json\ncontent-length:${msg.length}\nmessage-id:${_.uniqueId()}\n\n${msg}\0`);
                    }
                });
            }, 1000);
        }
        else {
            // clearInterval(itv);
            // conn.close();
        }
    });


    conn.on('close', function() {
        clearInterval(itv);
    });

    conn.on('end', function() {
        clearInterval(itv);
    });
});


var server = http.createServer();
server.addListener('upgrade', function(req,res){
    debug('upgrade', req)
    res.end();
});

sockjs_server.installHandlers(server, {
    prefix:'/events'
});

debug(' [*] Listening on 0.0.0.0:9999' );
server.listen(9999, '0.0.0.0');
