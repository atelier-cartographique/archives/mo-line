/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var _ = require('lodash');
var debug = require('debug')('animate');

function px (n) {
    return _.floor(n) + 'px';
}



function Translate (element, start, end) {
    this.element = element;
    this.start = start;
    this.end = end;
    element.style.position = 'absolute';
    element.style.left = px(start[0]);
    element.style.top = px(start[1]);
}

Translate.prototype.frame = function (t) {
    var s = this.start,
        e = this.end,
        x = s[0] + ((e[0] - s[0]) * t),
        y = s[1] + ((e[1] - s[1]) * t);

    this.element.style.left = px(x);
    this.element.style.top = px(y);
};


function PixelAttribute (element, attr, start, end) {
    this.element = element;
    this.attr = attr;
    this.start = start;
    this.end = end;
    element.style[attr] = px(start);
}

PixelAttribute.prototype.frame = function (t) {
    var s = this.start,
        e = this.end,
        attr = this.attr,
        n = s + ((e - s) * t);

    this.element.style[attr] = px(n);
};

function Bump (element, maxScale) {
    this.element = element;
    this.maxScale = maxScale - 1;
}

Bump.prototype.frame = function (t) {
    var tt = t * 2,
        scale;
    if (tt <= 1) {
        scale =  1 + (tt * this.maxScale);
    }
    else {
        scale = 1 + ((2 - tt) * this.maxScale);
    }
    this.element.style.transform = 'scale3d(' + scale +',' + scale + ', 1)';
    this.element.style.webkitTransform = 'scale3d(' + scale +',' + scale + ', 1)';
};

function Animation (duration) {
    this.duration = duration || 600;
    this.items = [];
}

Animation.prototype.add = function (item) {
    this.items.push(item);
    return this;
};

Animation.prototype.run = function () {
    var duration = this.duration,
        startAnimation = null,
        items = this.items;

    function resolver (resolve, reject) {
        function animate (ts) {
            if (!startAnimation) {
                startAnimation = ts;
            }
            var elapsed = ts - startAnimation,
                progress = elapsed / duration;
            if (elapsed < duration) {

                _.forEach(items, function(item){
                    item.frame(progress);
                });
                window.requestAnimationFrame(animate);
            }
            else {
                resolve();
            }
        }
        window.requestAnimationFrame(animate);
    }

    return (new Promise(resolver));
};


module.exports = {
    'Animation': Animation,
    'Translate': Translate,
    'Bump': Bump,
    'PixelAttribute': PixelAttribute
};
