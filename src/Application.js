/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var _ = require('lodash'),
    Client = require('./Client'),
    Bank = require('./Bank'),
    Operation = require('./Operation');
var debug = require('debug')('application');

function Application () {
    this.bank = null;
    this.client = null;

    this.setBank();
    this.setClient();
}

Application.prototype.setBank = function () {
    var viewportElement = document.getElementById('viewport');
    this.bank = new Bank(viewportElement);
    this.bank.init();
};

Application.prototype.setClient = function () {
    var bank = this.bank,
        client = new Client();

    function log (op) {
        debug(op.name);
    }

    function operate (op) {
        if(op instanceof Operation.Operation) {
            function ack () {
                client.post('ACK');
            }
            bank.update(op).then(ack);
        }
    }

    client.addHandler(log);
    client.addHandler(operate);
    this.client = client;
};




module.exports = Application;
