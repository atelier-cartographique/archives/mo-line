/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


var _ = require('lodash'),
    Counter = require('./Counter'),
    Ticket = require('./Ticket'),
    Animate = require('./Animate'),
    SlotManager = require('./Slot'),
    config = require('./mo.config'),
    Sound = require('./Sound'),
    helpers = require('./helpers'),
    Operation = require('./Operation');

var debug = require('debug')('bank');

var px = helpers.px;

function Bank (container) {
    this.container = container;
    this.sm = new SlotManager(this.container);
    this.sound = new Sound();
}

Bank.prototype.init = function () {
    var container = this.container,
        sm = this.sm,
        slotWidth = sm.slotWidth,
        counters = {},
        story = config.STORY,
        counterNames = config.COUNTERS[story];

    function createCounter (id) {
        var counter = new Counter(id),
            node = counter.getNode();
        counters[id] = counter;
        node.style.width = px(slotWidth);
        container.appendChild(node);
        sm.stackCounter(counter);
    }
    _.forEach(counterNames, createCounter);

    this.counters = counters;

    // setInterval(function monitor () {
    //     var n = 0;
    //     sm.each(function(slot){
    //         if(slot.hasCounter()) {
    //             n += 1;
    //         }
    //     });
    //     debug('monitor free slots', n);
    // }, 2000);

    return this;
};


Bank.prototype.opNext = function (op) {
    var counterName = op.data.servicePointName,
        ticketOrder = op.data.ticket,
        counter = this.counters[counterName],
        ticket = new Ticket(op.data.visitId, ticketOrder),
        sm = this.sm;

    this.sound.play();

    if (this.sm.isVisible(counter)) {
        var containerRect = this.container.getBoundingClientRect(),
            start = containerRect.bottom,
            end = 0,
            pa = new Animate.PixelAttribute(ticket.getNode(), 'marginTop',
                                             start, end);
        return counter.updateTicket(ticket, pa);
    }

    var showCounter = _.partial(sm.showCounter.bind(sm), counter);
    return counter.updateTicket(ticket, false).then(showCounter);
};


Bank.prototype.opEnd = function (op) {
    var vid = op.data.visitId;

    function clearCounter (counter) {
        var ticket = counter.getTicket();
        if (ticket && (vid === ticket.vid)) {
            counter.removeTicket();
            this.sm.stackCounter(counter);
        }
    }
    _.forEach(this.counters, clearCounter.bind(this));
    return Promise.resolve(vid);
};

Bank.prototype.opRecall = function (op) {
    var counterName = op.data.servicePointName,
        ticketOrder = op.data.ticket,
        counter = this.counters[counterName],
        ticket = new Ticket(op.data.visitId, ticketOrder),
        sm = this.sm;

    this.sound.play();

    if (this.sm.isVisible(counter)) {
        var bump = new Animate.Bump(ticket.getNode(), 1.16);
        return counter.updateTicket(ticket, bump);
    }

    var showCounter = _.partial(sm.showCounter.bind(sm), counter);
    return counter.updateTicket(ticket).then(showCounter);
};

Bank.prototype.update = function (op) {
    if (op instanceof Operation.VisitNext) {
        return this.opNext(op);
    }
    else if (op instanceof Operation.VisitEnd) {
        return this.opEnd(op);
    }
    else if (op instanceof Operation.VisitCall) {
        return this.opRecall(op);
    }
    return Promise.reject('Operation Not Handled');
};

module.exports = Bank;
