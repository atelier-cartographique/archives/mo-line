/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var _ = require('lodash'),
    Operation = require('./Operation');
var debug = require('debug')('client');
function Client () {
    this.service = new Worker('service.js');
    this.handlers = [];

    function onMessage (message) {
        try {
            var op = Operation.create(message.data);
        }
        catch (err) {
            debug('Client.onMessage', err, message);
        }

        _.each(this.handlers, function (handler) {
            handler(op);
        });
    }

    this.service.onmessage = onMessage.bind(this);
}

Client.prototype.post = function (message) {
    this.service.postMessage(message);
};

Client.prototype.addHandler = function (handler) {
    this.handlers.push(handler);
};


module.exports = Client;
