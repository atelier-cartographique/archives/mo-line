/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var helpers = require('./helpers'),
    Ticket = require('./Ticket'),
    Animate = require('./Animate'),
    config = require('./mo.config');
var debug = require('debug')('counter');
var DOM = helpers.DOM;

/**
<section class="ticket-infos">
    <div class="title fr">guichet</div>
    <div class="title nl">loket</div>
    <div class="guichet">
        <span class="level-nbr">017</span>
    </div>
    <!-- ticket placeholder -->
</section>
*/

var floorMapName = {'0': '⁰', '1': '¹'};

function formatCounterName (id) {
    var parts = id.split(config.COUNTER_NAME_SEP);
    var floor = floorMapName[parts[1]];
    var count = parts[2];
    return floor + count;
}

function template (data) {
    var root = document.createElement('div'),
        titleFr = document.createElement('div'),
        titleNl = document.createElement('div'),
        counterBlock = document.createElement('div'),
        counterName = document.createElement('span');

    DOM.addClass(root, 'ticket-infos');
    DOM.addClass(titleFr, 'title fr');
    DOM.addClass(titleNl, 'title nl');
    DOM.addClass(counterBlock, 'guichet');
    DOM.addClass(counterName, 'level-nbr');

    DOM.appendText(titleFr, 'guichet');
    DOM.appendText(titleNl, 'loket');
    DOM.appendText(counterName, formatCounterName(data.id));

    root.appendChild(titleFr);
    root.appendChild(titleNl);
    root.appendChild(counterBlock);
    counterBlock.appendChild(counterName);

    return root;
}


function Counter (id) {
    Object.defineProperty(this, 'id', {'value': id});
    this.node = template({'id': id});
    this.currentTicket = null;
}

Counter.prototype.getNode = function () {
    return this.node;
};

Counter.prototype.getTicket = function () {
    return this.currentTicket;
};

Counter.prototype.removeTicket = function () {
    if (this.currentTicket) {
        DOM.removeElement(this.currentTicket.getNode());
        this.currentTicket = null;
    }
};

Counter.prototype.updateTicket = function (ticket, anim) {
    function resolver (resolve, reject) {
        this.removeTicket();
        this.currentTicket = ticket;
        this.node.appendChild(ticket.getNode());
        if (!anim) {
            resolve();
        }
        else {
            var animRunner = new Animate.Animation();
            // var bmp = new Animate.Bump(ticket.getNode(), 1.3);
            animRunner.add(anim);
            animRunner.run().then(resolve);
        }
    }
    return (new Promise(resolver.bind(this)));
};

module.exports = Counter;
