/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var _ = require('lodash'),
    EventReceiver = require('./EventReceiver'),
    Queue = require('./Queue'),
    config = require('./mo.config');
var debug = require('debug')('event_provider');

/*
    Calls are emited in a weird fashion, like for a 're-call'
    operation as well as for a 'next' one. Thus the need to
    discriminate between them by keeping track of created
    visits and whether they've been nexted yet.
*/

var VISIT_STATE = {
    'CREATED': 0,
    'STARTED': 1,
    'FINISHED': 2,
    'NONE': 7
};
Object.freeze(VISIT_STATE);

function getFloor (id) {
    var parts = id.split(config.COUNTER_NAME_SEP);
    return parts[1];
}

function VisitTracker () {
    this.visits = {};
}

VisitTracker.prototype.getState = function (id) {
    if (id in this.visits) {
        return this.visits[id];
    }
    debug('VisitTracker.get: unknown visit');
    return VISIT_STATE.NONE;
};

VisitTracker.prototype.create = function (id) {
    if (id in this.visits) {
        debug('VisitTracker.create: visit already tracked', id);
        return;
    }
    this.visits[id] = VISIT_STATE.CREATED;
};

VisitTracker.prototype.start = function (id) {
    if (id in this.visits) {
        this.visits[id] = VISIT_STATE.STARTED;
        return;
    }
    debug('VisitTracker.start: unknown visit');
};

VisitTracker.prototype.end = function (id) {
    if (id in this.visits) {
        this.visits[id] = VISIT_STATE.ENDED;
        return;
    }
    debug('VisitTracker.end: unknown visit');
};



function EventProvider (onMessage, postMessage) {
    onMessage(this.onIfaceMessage.bind(this));
    Object.defineProperty(this, 'postMessage', {'value': postMessage});
    this.currentAck = null;
    this.tracker = new VisitTracker();
}

EventProvider.prototype.start = function () {
    this.queue = new Queue({
        'callback': this.processQueueItem.bind(this)
    });

    this.recv = new EventReceiver({
        'VISIT_CREATE': this.wrapMethod('onVisitCreate'),
        'VISIT_NEXT': this.wrapMethod('onVisitNext'),
        'VISIT_END': this.wrapMethod('onVisitEnd'),
        'VISIT_CALL': this.wrapMethod('onVisitCall'),
    });

    this.recv.connect();
    return this;
};

EventProvider.prototype.processQueueItem = function (item, ack) {
    var message = item.op.toObject();
    this.currentAck = ack;
    this.postMessage(message);
};

EventProvider.prototype.onIfaceMessage = function (message) {
    if (_.isFunction(this.currentAck)) {
        this.currentAck();
        this.currentAck = null;
    }
};

EventProvider.prototype.wrapMethod = function (methodName) {
    var method = this[methodName].bind(this);
    return method;
};

EventProvider.prototype.onVisitCreate = function (op) {
    this.tracker.create(op.data.visitId);
};

EventProvider.prototype.onVisitNext = function (op) {
    this.tracker.start(op.data.visitId);
    var story = getFloor(op.data.servicePointName);
    if (story === config.STORY) {
        this.queue.push(op);
    }
    else {
        debug('wrong story', story, config.STORY);
    }
};

EventProvider.prototype.onVisitEnd = function (op) {
    this.tracker.end(op.data.visitId);
    this.queue.push(op);
};

EventProvider.prototype.onVisitCall = function (op) {
    var story = getFloor(op.data.servicePointName);
    if (story === config.STORY) {
        var vs = this.tracker.getState(op.data.visitId);
        if (VISIT_STATE.STARTED === vs) {
            this.queue.push(op);
        }
    }
};


module.exports = EventProvider;
