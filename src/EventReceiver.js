/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var _ = require('lodash'),
    SockJS = require('sockjs-client'),
    Stomp = require('./vendors/stomp').Stomp,
    Operation = require('./Operation'),
    config = require('./mo.config');
var debug = require('debug')('event_receiver');
function defaultHandler (event) {
    return function (data) {
        debug('[[' + event + ']]', data);
    };
}


function EventReceiver (callbacks) {
    var handlers = {};
    _.forEach(Operation.STOMP_EVENT, function(EVENT) {
        if (EVENT in callbacks) {
            handlers[EVENT] = callbacks[EVENT];
        }
        else {
            handlers[EVENT] = defaultHandler(EVENT);
        }
    });
    this.handlers = handlers;
}

EventReceiver.prototype.onMessage = function (message) {
    var dbg = JSON.parse(message.body);
    var op = Operation.fromEvent(message.body);
    // console.log(dbg.eventName, dbg.parameters, op);
    if (op && (op.name in this.handlers)) {
        try {
            this.handlers[op.name](op);
        }
        catch (error) {
            debug('EventReceiver.onMessage', op);
        }
    }
    else {
        debug('EventReceiver.onMessage No Handlers', op);
    }
};

EventReceiver.prototype.onConnect = function (frame) {
    debug('CONNECTED', frame);
    this.client.subscribe(config.STOMP.sub, this.onMessage.bind(this));
};

EventReceiver.prototype.connect = function () {
    if (config.EVENT_GEN) {
        var generator = require('./generator');
        var that = this;
        var itv = setInterval(function(){
            _.each(generator(), function(g) {
                if ('STOP' === g) {
                    clearInterval(itv);
                }
                else {
                    that.onMessage({'body': JSON.stringify(g)});
                }
            });
        }, 2000);
        return;
    }
    this.socket = new SockJS(config.STOMP.sock);
    this.client = Stomp.over(this.socket);
    var headers = config.STOMP.headers;
    this.client.connect(headers, this.onConnect.bind(this));
};





module.exports = EventReceiver;
