/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var _ = require('lodash');
var debug = require('debug')('operation');

var STOMP_EVENT = [
    // 'USER_SESSION_START',
    // 'SERVICE_POINT_OPEN',
    // 'USER_SERVICE_POINT_SESSION_START',
    // 'USER_SERVICE_POINT_WORK_PROFILE_SET',
    // 'SERVICE_POINT_CLOSE',
    // 'USER_SERVICE_POINT_SESSION_END',
    // 'USER_SESSION_END',
    'VISIT_CREATE',
    'VISIT_REMOVE',
    'VISIT_NEXT',
    'VISIT_CALL',
    'VISIT_CONFIRM',
    'VISIT_RECYCLE',
    'VISIT_NOSHOW',
    'VISIT_TRANSFER_TO_QUEUE',
    'VISIT_TRANSFER_TO_SERVICE_POINT_POOL',
    'VISIT_TRANSFER_TO_USER_POOL',
    'VISIT_END',
    // 'QUEUE_POPULATED'
];

var PROTOTYPES = {};
PROTOTYPES = {
    'VISIT_CREATE': VisitCreate,
    'VISIT_NEXT': VisitNext,
    'VISIT_END': VisitEnd,
    'VISIT_CALL': VisitCall,

};

function Operation (data) {
    if (data) {
        this.data = data;
        Object.freeze(this.data);
    }
}

Operation.prototype.toObject = function () {
    return {
        'name': this.name,
        'data': this.data
    };
};

Operation.prototype.create = function (obj) {
    try {
        var Proto = PROTOTYPES[obj.name];
        return (new Proto(obj.data));
    }
    catch (err) {
        debug('Operation.create', obj.name, err);
        return null;
    }
};


Operation.prototype.fromEvent = function (event) {
    try {
        var data = JSON.parse(event),
            Proto = PROTOTYPES[data.eventName],
            parameters = _.pick(data.parameters, Proto.prototype.mkeys);
        var obj = new Proto(parameters);
    }
    catch (err) {
        debug('Operation.fromEvent', data.eventName);
        return null;
    }
    return obj;
};



function createProto (constructor, name, keys) {
    constructor.prototype = _.create(Operation.prototype, {
        'constructor': constructor,
        'name': name,
        'mkeys': ['visitId'].concat(keys)
    });

    PROTOTYPES[name] = constructor;
}


function VisitCreate () {
    Operation.apply(this, arguments);
}

function VisitNext () {
    Operation.apply(this, arguments);
}

function VisitEnd () {
    Operation.apply(this, arguments);
}

function VisitCall () {
    Operation.apply(this, arguments);
}

createProto(VisitCreate, 'VISIT_CREATE', ['serviceIntName', 'ticket']);
createProto(VisitNext, 'VISIT_NEXT', ['servicePointName', 'ticket']);
createProto(VisitEnd, 'VISIT_END', ['ticket']);
createProto(VisitCall, 'VISIT_CALL', ['servicePointName', 'ticket']);


module.exports.STOMP_EVENT = STOMP_EVENT;
module.exports.Operation = Operation;
module.exports.fromEvent = Operation.prototype.fromEvent;
module.exports.create = Operation.prototype.create;
module.exports.VisitCreate = VisitCreate;
module.exports.VisitNext = VisitNext;
module.exports.VisitEnd = VisitEnd;
module.exports.VisitCall = VisitCall;
