/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


var _ = require('lodash');
var debug = require('debug')('queue');

var queueDefaults = {
    'timeout': 12000,
    'frequency': 100
};

var Q_STATE = {
    'OPEN': 0,
    'WAITING': 0
};
Object.freeze(Q_STATE);


function QueueItem (op) {
    this.id = _.uniqueId('qi_');
    this.ts = _.now();
    this.op = op;
    Object.freeze(this);
}

function Queue (options) {
    _.defaults(options, queueDefaults);
    this.timeout = options.timeout;
    this.callback = options.callback;
    this.state = Q_STATE.OPEN;
    this.ts = -1;
    this.ack = null;
    this.q = [];
    setInterval(this.release.bind(this), options.frequency);
}

Queue.prototype.push = function (op) {
    var item = new QueueItem(op);
    this.q.push(item);
};

Queue.prototype.resetState = function () {
    this.ack = null;
    this.ts = -1;
    this.state = Q_STATE.OPEN;
};

Queue.prototype.acker = function (id) {
    function ack () {
        // console.log('ack', id, this.ack);
        if (id === this.ack) {
            this.resetState();
        }
    };
    return ack.bind(this);
};

Queue.prototype.release = function () {
    if (Q_STATE.WAITING === this.state) {
        var elapsed = _.now() - this.ts;
        if (elapsed < this.timeout) {
            return;
        }
        this.resetState();
    }
    if (this.q.length > 0) {
        var item = this.q.shift();
        this.ack = item.id;
        this.ts = _.now();
        try {
            this.callback(item, this.acker(item.id));
        }
        catch (err) {
            this.resetState();
        }
    }
};


module.exports = Queue;
