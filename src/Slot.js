/**
* mo-line manages display of Molenbeek's line system
* Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
*
* This program is free software: you can redistribute
* it and/or modify it under the terms of the GNU Affero
* General Public License as published by the Free Software Foundation,
* either version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty
* of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* See the GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


var _ = require('lodash'),
    Counter = require('./Counter'),
    Ticket = require('./Ticket'),
    Animate = require('./Animate'),
    helpers = require('./helpers'),
    config = require('./mo.config');
var debug = require('debug')('slot');

var px = helpers.px;

function positionNode (node, pos) {
    node.style.position = 'absolute';
    node.style.left = px(pos[0]);
    node.style.top = px(pos[1]);
}

function Slot (pos) {
    this.pos = pos;
    this.ts = -1;
    this.counter = null;
}

Slot.prototype.free = function () {
    this.counter = null;
    this.ts = -1;
};

Slot.prototype.getPosition = function () {
    return [this.pos[0], this.pos[1]];
};

Slot.prototype.getTS = function () {
    return this.ts;
};

Slot.prototype.getCounter = function () {
    return this.counter;
};

Slot.prototype.setCounter = function (counter) {
    this.counter = counter;
    this.ts = _.now();
};

Slot.prototype.hasCounter = function (counter) {
    if (!this.counter) {
        return false;
    }
    else if (!counter) {
        return !!(this.counter);
    }
    return (this.counter.id === counter.id);
};

function SlotManager (container) {
    this.container = container;
    this.capacity = config.SLOTS;
    this.stack = {};
    this.monitors = {};
    this.setGeometry();
    this.visibles = new Array();
    for (var i = 0; i < this.capacity; i++) {
        var pos = [
            (this.slotWidth * i),
            this.containerRect.top
        ];
        this.visibles.push(new Slot(pos));
    }


}


SlotManager.prototype.setGeometry = function () {
    var containerRect = this.container.getBoundingClientRect(),
        slotWidth = _.floor(containerRect.width / this.capacity),
        slotHeight = containerRect.height;
    this.containerRect = containerRect;
    this.slotWidth = slotWidth;
    this.slotHeight = slotHeight;
    this.stackPosition = [containerRect.right, containerRect.top];
};

SlotManager.prototype.each = function (fn) {
    _.each(this.visibles, fn.bind(this));
};

SlotManager.prototype.getSlot = function (idx) {
    return this.visibles[idx];
};

SlotManager.prototype.addCounter = function (counter) {
    var node = counter.getNode();
    this.stack[counter.id] = counter;
    positionNode(node, this.stackPosition);
};

SlotManager.prototype.isVisible = function (counter) {
    var visible = false;
    this.each(function(slot){
        if (slot.hasCounter(counter)) {
            visible = true;
        }
    });
    return visible;
};


SlotManager.prototype.setAnimation = function (anim) {
    var freeSlot = null;
    this.each(function (slot) {
        if (!freeSlot) {
            if (!slot.hasCounter()) {
                freeSlot = slot;
                return;
            }
        }
        else {
            if (slot.hasCounter()) {
                var currentCounter = slot.getCounter(),
                    startPos = slot.getPosition();
                slot.free();

                var node = currentCounter.getNode(),
                    endPos = freeSlot.getPosition();
                anim.add(new Animate.Translate(node, startPos, endPos));

                freeSlot.setCounter(currentCounter);
                freeSlot = slot;
            }
        }
    });
    return freeSlot;
};

SlotManager.prototype.stackCounter = function (counter) {
    this.each(function(slot){
        if (slot.hasCounter(counter)) {
            slot.free();
        }
    });
    positionNode(counter.getNode(), this.stackPosition);
    var anim = new Animate.Animation();
    this.setAnimation(anim);
    anim.run();
};

SlotManager.prototype.reclaimSlot = function () {
    var candidate = null;
    this.each(function(slot, idx){
        var ts = slot.getTS();
        if (!candidate || (ts < candidate.getTS())) {
            candidate = slot;
            debug('reclaimSlot', idx);
        }
    });
    if (candidate.hasCounter()) {
        this.stackCounter(candidate.getCounter());
    }
    return candidate;
};

SlotManager.prototype.showCounter = function (counter) {
    debug('show', counter.id);
    if (this.isVisible(counter)) {
        return (Promise.resolve(counter));
    }
    function resolver (resolve, reject) {
        this.reclaimSlot();

        var anim = new Animate.Animation();
        var slot = this.setAnimation(anim);

        anim.add(new Animate.Translate(
                        counter.getNode(),
                        this.stackPosition,
                        slot.getPosition()));
        slot.setCounter(counter);
        anim.run().then(resolve);
    }
    return (new Promise(resolver.bind(this)));
};

module.exports = SlotManager;
