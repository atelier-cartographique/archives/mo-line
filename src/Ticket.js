/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var helpers = require('./helpers');
var debug = require('debug')('ticket');

var DOM = helpers.DOM;

/**
 <div class="ticket">
     <span class="level-nbr">0247</span>
 </div>
*/

var floorMapName = {'Y': '⁰', 'Z': '¹'};

function formatTicketName (id) {
    var floor = floorMapName[id.slice(0 ,1)];
    var ticketNumber = id.slice(1);
    return floor + ticketNumber;
}

function template (data) {
    var root = document.createElement('div'),
        ticketNumber = document.createElement('span');

    DOM.addClass(root, 'ticket');
    DOM.addClass(ticketNumber, 'level-nbr');

    DOM.appendText(ticketNumber, formatTicketName(data.id));

    root.appendChild(ticketNumber);

    return root;
}



function Ticket (vid, id) {
    Object.defineProperty(this, 'vid', {'value': vid});
    Object.defineProperty(this, 'id', {'value': id});
    this.node = template({'id': id});
}


Ticket.prototype.getNode = function () {
    return this.node;
};

module.exports = Ticket;
