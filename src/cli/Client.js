/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


const Operation = require('../Operation');
const EventProvider = require('../EventProvider');
const debug = require('debug')('client');

class Client {
    constructor () {
        this.handlers = [];
        const listen =  message => {
            debug(message);
            try {
                var op = Operation.create(message);
            }
            catch (err) {
                console.error('Client.onMessage', err, message);
            }

            this.handlers.forEach((handler) => {
                handler(op);
            });
        };

        const send = cb => {
            Object.defineProperty(this, '_send', {
                'value': cb
            });
        };

        const provider = new EventProvider(send, listen);
        provider.start();
    }

    post (message) {
        if (this._send) {
            this._send(message);
        }
    }

    addHandler (handler) {
        this.handlers.push(handler);
    }
}


module.exports = Client;
