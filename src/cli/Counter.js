/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var Ticket = require('./Ticket');

class Counter {
    constructor (id) {
        Object.defineProperty(this, 'id', {'value': id});
        this.currentTicket = null;
    }

    getTicket () {
        return this.currentTicket;
    }

    removeTicket () {
        if (this.currentTicket) {
            this.currentTicket = null;
        }
    }

    updateTicket (ticket, anim) {
        const resolver = (resolve, reject) => {
            this.removeTicket();
            this.currentTicket = ticket;
            resolve();
        };
        return (new Promise(resolver));
    }
}



module.exports = Counter;
