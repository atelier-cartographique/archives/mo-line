/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


const FC = Object.freeze([
    ['SendToInitialSegment', 0x00],
    ['SetClock', 0x01],
    ['ClearAllSegments', 0x07],
    ['SendPing', 0x20],
    ['RequestForTransmissionResult', 0x21],
    ['ResponseForTransmissionResult', 0x31]
]);


const CC = Object.freeze([
    ['ClearBuffer', 0x01],
    ['ShowTextString', 0x03],
    ['ShowCurrentTime', 0x04],
    ['ShowCurrentDate', 0x05],
    ['Delay', 0x06],
    ['DisplayBuffer', 0x07],
    ['ShowTextImmediately', 0x08],
    ['ShowTemperature', 0x0b],
    ['EndOfSegementData', 0x1f]
]);



exports.FC = FC.reduce((m, v) => {
    m[v[0]] = v[1];
    return m;
}, {});

exports.CC = CC.reduce((m, v) => {
    m[v[0]] = v[1];
    return m;
}, {});

exports.comName = function (code) {
    try {
        return CC.find(r => r[1] === code)[0];
    }
    catch (err) {
        return `${err} UnknownCommand ${code}`;
    }
};
