/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const debug = require('debug')('message');
const {CC} = require('./Lingua');

const HEAD_SIZE = 9;
const FRAME_SIZE = 4000;

const FONT = 0x00;
const FCOLOR = 0x01;
const BGCOLOR = 0x00;


// function readFunctionCode (buf) {
//     const code = ui8(buf);
//     let fc = FC.filter(x => x[1] === code);
//     return fc[0];
// }

// function writeFunctionCode (fc, buf) {
//     let code = FC.filter(x => x[0] === fc);
//     return buf.writeUInt8(code);
// }

// function mkTuple (hi, low) {
//     return Buffer.from([hi, low]);
// }
//
// function getHi (tuple) {
//     return tuple.slice(0, 1);
// }
//
// function getLow (tuple) {
//     return tuple.slice(1, 2);
// }
//
function ui8 (buf, idx=0) {
    return buf.readUInt8(idx);
}

function ui16 (buf, idx=0) {
    return buf.readUInt16BE(idx);
}

// function eq (b1, b2) {
//     return ui8(b1) === ui8(b2);
// }
//
// function xor (b1, b2) {
//     return ui8(b1) ^ ui8(b2);
// }

function checksum (slice) {
    return slice.reduce((prev, cur) => prev ^ cur, 0);
}

function hi (n) {
    return (0x0000ff00 & n) >> 8;
}

function low (n) {
    return (0x000000ff & n);
}

function bound (sz) {
    const max = Math.pow(2, sz) - 1;
    return function (n) { return Math.min(max, n); };
}

let bound8 = bound(8);
let bound16 = bound(16);


class Message {
    constructor () {
        Object.defineProperty(this, 'buffer', {
            value: Buffer.alloc(FRAME_SIZE - (HEAD_SIZE + 1))
        });

        Object.defineProperty(this, 'offset', {
            value: 0,
            writable: true,
            configurable: false,
            enumerable: false
        });

    }

    write (data) {
        data.forEach(code => {
            try {
                this.offset = this.buffer.writeUInt8(code, this.offset);
                // debug('write', this.offset, code);
            } catch (e) {
                debug(e);
            }
        });
        return this;
    }

    functionCode (fc) {
        // this.len = writeFunctionCode(fc, this.buffer);
        return this.write([fc]);
    }

    clearBuffer () {
        return this.write([0x01]);
    }

    text (x, y, str) {
        let data = [CC.ShowTextString,
                    FONT, FCOLOR, BGCOLOR, x, y];
        for (let i = 0; i < str.length; i++) {
            const c = str.charCodeAt(i);
            if ((c > 0) && (c < 256)) {
                data.push(c);
            }
        }
        data.push(0x00);
        return this.write(data);
    }

    delay (time=0) {
        time = bound16(time);
        const data = [
            CC.Delay,
            hi(time),
            low(time)
        ];
        return this.write(data);
    }

    display (mode=0, speed=0) {
        const data = [
            CC.DisplayBuffer,
            0x00,
            0x00,
            mode,
            speed
        ];
        return this.write(data);
    }

    end () {
        this.write([CC.EndOfSegementData]);
        return this.write([checksum(this.buffer.slice(0, this.offset))]);
    }

}


function formatMessage(sender, receiver, message) {
    sender = bound16(sender);
    receiver = bound16(receiver);

    const length = message.offset;
    let buf = Buffer.alloc(HEAD_SIZE + length);
    debug('formatMessage:', 'length', length);

    // protocolId
    buf.writeUInt8(0xaa, 0);
    buf.writeUInt8(0xbb, 1);

    // senderId
    buf.writeUInt8(hi(sender), 2);
    buf.writeUInt8(low(sender), 3);

    // receiverId
    buf.writeUInt8(hi(receiver), 4);
    buf.writeUInt8(low(receiver), 5);

    // message length (witout checksum)
    buf.writeUInt8(hi(length - 1), 6);
    buf.writeUInt8(low(length - 1), 7);

    // header checksum
    buf.writeUInt8(checksum(buf.slice(0, 8)), 8);

    // message.body
    message.buffer.copy(buf, 9, 0, length);

    return buf;
}

function readMessage(buf) {
    // sender = bound16(sender);
    // receiver = bound16(receiver);
    //
    // const length = message.offset;
    // let buf = Buffer.alloc(HEAD_SIZE + length);
    // debug('formatMessage:', 'length', length);

    // protocolId
    const pid = ui16(buf, 0);

    // senderId
    const sid = ui16(buf, 2);

    // receiverId
    const rid = ui16(buf, 4);

    // message length (witout checksum)
    const length = ui16(buf, 6);

    // header checksum
    const hcs = ui8(buf, 8);

    // message.body
    const body = buf.slice(9, 9 + length);

    return {
        pid, sid, rid, body
    };
}



/*
 put exports here in order to isolate bits of not-es6-ready bits
 in a dumb enough form that it could easily be se'ed at some point
 */

exports.Message = Message;
exports.formatMessage = formatMessage;
exports.readMessage = readMessage;
