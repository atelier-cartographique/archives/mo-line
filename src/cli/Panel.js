/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const {Message, formatMessage} = require('./Message');
const {FC} = require('./Lingua');
const log = require('debug')('panel');
const  _ = require('lodash'),
    Counter = require('./Counter'),
    Ticket = require('./Ticket'),
    SlotManager = require('./Slot'),
    config = require('./mo.config'),
    Operation = require('../Operation');


class Panel {
    constructor (device) {
        Object.defineProperty(this, 'device', {value: device});
        this.sm = new SlotManager(config.SLOT_WIDTH);
        this.prefix = config.PANEL_PREFIX;
    }

    init () {
        const sm = this.sm,
            slotWidth = sm.slotWidth,
            counters = {},
            story = config.STORY,
            counterNames = config.COUNTERS[story];

        counterNames.forEach((id) => {
            const counter = new Counter(id);
            counters[id] = counter;
            sm.stackCounter(counter);
        });

        this.counters = counters;
        return this;
    }

    update (op) {
        log(op);
        const updateDevice = pr => {
            log('updateDevice');
            return pr.then(() => {
                const prefix = this.prefix,
                    offset = prefix.length,
                    sm = this.sm;

                var msg = new Message();
                msg.functionCode(FC.SendToInitialSegment)
                    .clearBuffer()
                    .text(0, 0, prefix);

                sm.each(slot => {
                    if (slot.hasCounter()) {
                        const pos = slot.getPosition(),
                            counter = slot.getCounter(),
                            ticket = counter.getTicket();
                        if (ticket) {
                            msg.text(pos + offset, 0, ticket.id.substr(1));
                        }
                    }
                });
                msg.display();
                return Promise.resolve(
                    this.device.write(formatMessage(1, 0, msg))
                );
            });
        };

        if (op instanceof Operation.VisitNext) {
            return updateDevice(this.opNext(op));
        }
        else if (op instanceof Operation.VisitEnd) {
            return updateDevice(this.opEnd(op));
        }
        else if (op instanceof Operation.VisitCall) {
            return updateDevice(this.opRecall(op));
        }
        return Promise.reject('Operation Not Handled');
    }

    opNext (op) {
        const counterName = op.data.servicePointName,
            ticketOrder = op.data.ticket,
            counter = this.counters[counterName],
            ticket = new Ticket(op.data.visitId, ticketOrder),
            sm = this.sm;

        if (this.sm.isVisible(counter)) {
            return counter.updateTicket(ticket);
        }

        return counter.updateTicket(ticket).then(() => sm.showCounter(counter));
    };

    opEnd (op) {
        const vid = op.data.visitId;
        Object.keys(this.counters).forEach(k => {
            const counter = this.counters[k],
                ticket = counter.getTicket();
            if (ticket && (vid === ticket.vid)) {
                // this.sm.stackCounter(counter);
                counter.removeTicket();
            }
        });
        return Promise.resolve(vid);
    };

    opRecall (op) {
        const counterName = op.data.servicePointName,
            ticketOrder = op.data.ticket,
            counter = this.counters[counterName],
            ticket = new Ticket(op.data.visitId, ticketOrder),
            sm = this.sm;

        if (this.sm.isVisible(counter)) {
            return counter.updateTicket(ticket);
        }

        return counter.updateTicket(ticket).then(() => sm.showCounter(counter));
    };
}

module.exports = Panel;
