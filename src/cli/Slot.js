/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


const _ = require('lodash'),
    Counter = require('./Counter'),
    Ticket = require('./Ticket'),
    config = require('./mo.config');


class Slot {
    constructor (pos) {
        this.pos = pos;
        this.ts = -1;
        this.counter = null;
    }

    free () {
        this.counter = null;
        this.ts = -1;
    }

    getPosition () {
        return this.pos;
    }

    getTS () {
        return this.ts;
    }

    getCounter () {
        return this.counter;
    }

    setCounter (counter) {
        this.counter = counter;
        this.ts = _.now();
    }

    hasCounter (counter) {
        if (!this.counter) {
            return false;
        }
        else if (!counter) {
            return !!(this.counter);
        }
        return (this.counter.id === counter.id);
    }
}

class SlotManager {
    constructor (slotWidth) {
        Object.defineProperty(this, 'slotWidth', {value: slotWidth});
        this.capacity = config.SLOTS;
        this.stack = {};
        this.visibles = new Array();
        for (let i = 0; i < this.capacity; i++) {
            this.visibles.push(new Slot(this.slotWidth * i));
        }
    }

    each (fn) {
        this.visibles.forEach(fn);
    }

    getSlot (idx) {
        return this.visibles[idx];
    }

    addCounter (counter) {
        this.stack[counter.id] = counter;
    }

    isVisible(counter) {
        let visible = false;
        this.each(slot => {
            if (slot.hasCounter(counter)) {
                visible = true;
            }
        });
        return visible;
    }

    stackCounter (counter) {
        this.each(slot => {
            if (slot.hasCounter(counter)) {
                slot.free();
            }
        });
    }

    reclaimSlot () {
        // lookup free slot
        let candidate = null;
        this.each(slot => {
            const ts = slot.getTS();
            if (!candidate || (ts < candidate.getTS())) {
                candidate = slot;
            }
        });
        if (candidate.hasCounter()) {
            this.stackCounter(candidate.getCounter());
        }
        return candidate;
    }

    showCounter (counter) {
        if (this.isVisible(counter)) {
            return (Promise.resolve(counter));
        }

        const resolver = (resolve, reject) => {
            let freeIdx = -1;
            this.reclaimSlot();

            this.each((slot, idx) => {
                if (freeIdx < 0) {
                    if (!slot.hasCounter()) {
                        freeIdx = idx;
                        return;
                    }
                }
                else {
                    if (slot.hasCounter()) {
                        const currentCounter = slot.getCounter();
                        slot.free();
                        this.getSlot(idx - 1).setCounter(currentCounter);
                        freeIdx = idx;
                    }
                    else {
                        freeIdx = idx;
                    }
                }
            });

            this.getSlot(freeIdx).setCounter(counter);
            resolve();
        };
        return (new Promise(resolver));
    };

}

module.exports = SlotManager;
