/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const {FC, CC, comName} = require('./Lingua');
const {readMessage} = require('./Message');
const {curry} = require('lodash/fp');
const {createServer} = require('net');
const log = require('debug')('virtual');
const view = require('debug')('vpanel');

class State {
    constructor (s) {
        for (let k in s) {
            this[k] = s[k];
        }
        Object.freeze(this);
    }
}


function ns (s, n) {
    let r = new Array(n);
    for(let i = 0; i < n; i++) {
        r[i] = s;
    }
    return r.join('');
}

function* bufIterator (buffer) {
    let offset = 0;
    while (offset < buffer.length) {
        yield buffer.readUInt8(offset);
        offset += 1;
    }
}

function bufReader (buffer) {
    const iter = bufIterator(buffer);
    return function () {
        const {value, done} = iter.next();
        if (done) {
            return null;
        }
        return value;
    };
}


class Slot {
    constructor () {
        this.charcode = null;
    }

    clear () {
        this.charcode = null;
    }

    setFont (f) {

    }

    setBackground (c) {

    }

    setForeground (c) {

    }

    selectChar (c) {
        this.charcode = c;
    }
}

class Screen {
    constructor (w, h) {
        // log(`Sreen ${w} ${h}`);
        const lines = [];
        for (let i = 0; i < h; i++) {
            let l = [];
            for (let j = 0; j < w; j++) {
                l.push(new Slot());
            }
            lines.push(l);
        }
        this.lines = lines;
    }

    withSlot (x, y, method) {
        // log(this);
        const slot = this.lines[y][x] || new Slot();
        return method(slot, x, y);
    }

    each (method) {
        this.lines.forEach(l => l.forEach(method));
    }
}

class Panel {
    constructor (width = 20, height = 1) {
        Object.defineProperty(this, 'width', { value: width });
        Object.defineProperty(this, 'height', { value: height });
        this.screens = [new Screen(width, height), new Screen(width, height)];
    }

    get offscreen () { return this.screens[0]; }
    get onscreen  () { return this.screens[1]; }

    swapScreens () {
        const s = this.screens[0];
        this.screens[0] = this.screens[1];
        this.screens[1] = s;

        let disp = '|';
        this.onscreen.each(slot => {
            disp += String.fromCodePoint(slot.charcode || 32);
        });
        disp += '|';
        view(ns('-', this.width + 2));
        view(disp);
        view(ns('-', this.width + 2));
    }

    clear () {
        this.screens.forEach(screen => {
            screen.each(slot => slot.clear());
        });
    }

    setChar (charCode, x, y, state) {
        this.offscreen.withSlot(x, y, slot => {
            slot.setFont(state.font);
            slot.setBackground(state.background);
            slot.setForeground(state.foreground);
            slot.selectChar(charCode);
        });
    }

}


class CCTable {

    static [CC.ClearBuffer] (reader, panel) {
        panel.clear();
    }

    static [CC.ShowTextString] (reader, panel) {
        const font = reader(),
            fg = reader(),
            bg = reader(),
            x = reader(),
            y = reader();

        let text = [], cc = reader();
        while (0x00 !== cc) {
            // log(`ShowTextString ${cc}`);
            text.push(cc);
            cc = reader();
        }

        const state = new State({
            'font': font,
            'foreground': fg,
            'background': bg
        });
        let xx = x;
        text.forEach(code => {
            panel.setChar(code, xx, y, state);
            xx += 1;
        });
    }

    static [CC.DisplayBuffer] (reader, panel) {
        panel.swapScreens();
    }

    static [CC.EndOfSegementData] (reader) {
        // noop
    }
};


class Handler {
    constructor (sock, panel) {
        Object.defineProperty(this, 'panel', {'value': panel});
        sock.on('data', (buf) => this.onData(sock, buf) );
    }

    onData (sock, buf) {
        const msg = readMessage(buf);
        const body = msg.body;
        const fcode = body.readUInt8(0);

        if (FC.SendToInitialSegment === fcode) {
            this.run(body.slice(1));
        }
    }

    run (buffer) {
        let reader = bufReader(buffer);
        while (true) {
            let code, method;
            try {
                code = reader();
                if (code in CCTable) {
                    log(comName(code));
                    method = CCTable[code];
                    method(reader, this.panel);
                }
                else {
                    return;
                }
            }
            catch (err) {
                log(err);
                return false;
            }
        }
    }
}


function main () {
    const options = {
        host: 'localhost',
        port: 9001
    };

    const panel = new Panel(20, 1);

    const server = createServer((socket) => {
        socket.write('hello\n');
    });

    server.on('error', (err) => {
        throw err;
    });

    server.on('connection', (socket) => {
        const addr = socket.address();
        log(`Client Connected ${addr.address} ${addr.port}`);
        const h = new Handler(socket, panel);
        socket.on('end', () => {
            log(`Client End ${addr}`);
        });
    });

    server.listen(options);
}

exports.start = main;
