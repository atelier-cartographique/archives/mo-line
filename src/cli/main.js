/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const {Message, formatMessage} = require('./Message');
const {FC} = require('./Lingua');
const {start} = require('./VirtualDevice');
const {connect} = require('net');
const debug = require('debug')('mo');
const cluster = require('cluster');
const Client = require('./Client');
const Panel = require('./Panel');
const Operation = require('../Operation');

start();

// // debug(FC);

// // buf.forEach((v, i) => {
// //     debug(i + ': ' +v.toString(16));
// // });

// // let sentences = [
// //     "Hélas ! vers le passé tournant un œil d'envie,",
// //     "Sans que rien ici-bas puisse m'en consoler,",
// //     "Je regarde toujours ce moment de ma vie",
// //     "Où je l'ai vue ouvrir son aile et s'envoler!",
// //
// //     "Je verrai cet instant jusqu'à ce que je meure,",
// //     "L'instant, pleurs superflus !",
// //     "Où je criai : L'enfant que j'avais tout à l'heure,",
// //     "Quoi donc ! je ne l'ai plus !",
// // ];

// //
// // function mkMessage (op) {
// //     let msg = new Message();
// //     msg.functionCode(FC.SendToInitialSegment)
// //         .clearBuffer()
// //         .text(0, 0, op.data.ticket.toString())
// //         .display();
// //     return formatMessage(1, 0, msg);
// // }


// function connectStomp (device) {
//     const client = new Client(),
//         panel = new Panel(device);

//     function ack () {
//         client.post('ACK');
//     }

//     function log (op) {
//         debug('<<| ' + op.name);
//     }

//     function operate (op) {
//         try {
//             if(op instanceof Operation.Operation) {
//                 // device.write(mkMessage(op));
//                 // ack();
//                 panel.update(op)
//                     .then(ack)
//                     .catch(err => {
//                         debug(err);
//                         ack();
//                     });
//             }
//         }
//         catch (err) {
//             debug(err);
//         }
//     }

//     panel.init();
//     client.addHandler(log);
//     client.addHandler(operate);
// }


// /*if (cluster.isMaster) {
//     const worker = cluster.fork();
//     worker.on('message', msg => {
//         debug(`message from worker: ${msg}`);
//         if ('ready' === msg) {
//             let devClient = connect(9001, 'localhost', () => {

//                 debug('connected');

//                 devClient.on('end', () => {
//                     debug('connection end');
//                     process.exit(0);
//                 });

//                 devClient.on('data', (buf) => {
//                     debug(`connection data ${buf}`);
//                 });


//                 connectStomp(devClient);
//             });
//         }
//     });
// }
// else {
//     start();
//     process.send('ready');
// }*/

