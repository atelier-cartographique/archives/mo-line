


module.exports.STOMP = {
    'sock': 'http://10.102.38.118:8080/qpevents/events',
    'sub': '/topic/event/**',
    'headers': {}
};


module.exports.COUNTERS = {
    '?': ['?01', '?02', '?03', '?04', '?05'],
    '!': ['!01', '!02', '!03', '!04', '!05']
};

module.exports.STORY = '?';
module.exports.SLOTS = 3;
module.exports.PANEL_PREFIX = '?> ';
module.exports.SLOT_WIDTH = 6;

module.exports.EVENT_GEN = true;
