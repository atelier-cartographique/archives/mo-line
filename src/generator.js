/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var _ = require('lodash'),
    config = require('./mo.config');
var debug = require('debug')('generator');
var created = [];
var visited = [];

var SPECIALS = {
    '0': 'Y',
    '1': 'Z'
};

// var prefix = config.STORY;
var LINES = ['A', 'B', 'C'];

function ns (s, n) {
    var r = new Array(n);
    for(var i = 0; i < n; i++) {
        r[i] = s;
    }
    return r.join('');
}

function pad (source, c, n) {
    var diff = n - source.length;
    return ns(c, diff) + source;
}

function getStory() {
    var stories = Object.keys(config.COUNTERS);
    var sIdx = _.random(stories.length - 1);
    var story = stories[sIdx];
    return story;
}

function genMessageCreate () {
    var uid = _.uniqueId();
    var vid = Number(uid);
    if (vid >= 999) {
        return ['STOP'];
    }
    var prefix = SPECIALS[getStory()];
    var ticket = prefix + pad(uid, '0', 9);
    var event = {
        'eventName': 'VISIT_CREATE',
        'parameters': {
            'ticket': ticket,
            'visitId': vid
        }
    };
    debug('create', ticket);
    created.push(event);
    return [event];
    // return {'body': JSON.stringify(event)};
}

function genMessageNext () {
    if (0 === created.length) {
        return [];
    }

    var counters = config.COUNTERS[getStory()];
    var counter = counters[_.random(counters.length - 1)];

    var pc = _.filter(visited, function(e){
        return (e.parameters.servicePointName === counter);
    });

    visited = _.filter(visited, function(e){
        return (e.parameters.servicePointName !== counter);
    });

    var visit = created.splice(_.random(created.length - 1), 1)[0];
    var event = {
        'eventName': 'VISIT_NEXT',
        'parameters': {
            'servicePointName' : counter,
            'ticket': visit.parameters.ticket,
            'visitId': visit.parameters.visitId
        }
    };
    debug('next', counter, visit.parameters.ticket);
    var toSend = [];
    _.each(pc, function (e) {
        toSend.push({
            'eventName': 'VISIT_END',
            'parameters': {
                'ticket': e.parameters.ticket,
                'visitId': e.parameters.visitId
            }
        });
    });
    visited.push(event);
    toSend.push(event);
    // toSend.push('STOP');
    return toSend;
}

function genMessageCall () {
    if (0 === visited.length) {
        return [];
    }
    var visit = JSON.parse(JSON.stringify(_.sample(visited)));
    visit.eventName = 'VISIT_CALL';
    debug('call', visit.parameters.servicePointName, visit.parameters.ticket);
    return [visit];
}


function genMessageEnd () {
    if (0 === visited.length) {
        return [];
    }
    var visit = visited.splice(_.random(visited.length - 1), 1)[0];
    var event = {
        'eventName': 'VISIT_END',
        'parameters': {
            'ticket': visit.parameters.ticket,
            'visitId': visit.parameters.visitId
        }
    };
    debug('end', visit.parameters.ticket);
    // return [event, 'STOP'];
    return [event];
}

var generators = [];
var gi;
for (gi = 0; gi < 120; gi ++) { generators.push(genMessageCreate); }
for (gi = 0; gi < 80; gi ++) { generators.push(genMessageNext); }
for (gi = 0; gi < 40; gi ++) { generators.push(genMessageCall); }
for (gi = 0; gi < 40; gi ++) { generators.push(genMessageEnd); }

module.exports = function () {
    return generators[_.random(generators.length - 1)]();
};
