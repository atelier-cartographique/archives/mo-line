/*
 * helpers.js
 *
 *
 * Copyright (C) 2015  Pierre Marchand <pierremarc07@gmail.com>
 *
 * License in LICENSE file at the root of the repository.
 *
 */

'use strict';

var _ = require('lodash');
var debug = require('debug')('helpers');

module.exports.copy = function (data) {
    return JSON.parse(JSON.stringify(data));
};


module.exports.px = function (val) {
    val = val || 0;
    return _.floor(val).toString() + 'px';
};


// DOM

var DOMMethods = {
    'setAttributes': function (attrs) {
        var el = this;
        _.each(attrs, function (val, k) {
            el.setAttribute(k, val);
        });
    },

    'addClass': function (c) {
        var ecStr = this.getAttribute('class');
        var ec = ecStr ? ecStr.split(' ') : [];
        ec.push(c);
        this.setAttribute('class', _.uniq(ec).join(' '));
    },

    'toggleClass': function (c) {
        var ecStr = this.getAttribute('class');
        var ec = ecStr ? ecStr.split(' ') : [];
        if (_.indexOf(ec, c) < 0) {
            exports.DOM.addClass(this, c);
        }
        else {
            exports.DOM.removeClass(this, c);
        }
    },

    'hasClass': function (c) {
        var ecStr = this.getAttribute('class');
        var ec = ecStr ? ecStr.split(' ') : [];
        return !(_.indexOf(ec, c) < 0);
    },

    'removeClass': function (c) {
        var ecStr = this.getAttribute('class');
        var ec = ecStr ? ecStr.split(' ') : [];
        this.setAttribute('class', _.without(ec, c).join(' '));
    },

    'emptyElement': function () {
        while (this.firstChild) {
            exports.DOM.removeElement(this.firstChild);
        }
    },

    'removeElement': function (keepChildren) {
        if (!keepChildren) {
            exports.DOM.emptyElement(this);
        }
        var parent = this.parentNode,
            evt = document.createEvent('CustomEvent');
        parent.removeChild(this);
        evt.initCustomEvent('remove', false, false, null);
        this.dispatchEvent(evt);
    },

    'appendText': function (txt) {
        var textElement = document.createTextNode(txt.toString());
        this.appendChild(textElement);
    }
};

module.exports.DOM = {};

function wrapDOMMethod (fn, name) {
    function wrapped () {
        var args = Array.prototype.slice.call(arguments),
            elem = args.shift();
        var result = fn.apply(elem, args);
        if (result === undefined) {
            return elem;
        }
        return result;
    }

    module.exports.DOM[name] = wrapped;
}


_.each(DOMMethods, wrapDOMMethod);
