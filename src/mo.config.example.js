
function ns (s, n) {
    var r = new Array(n);
    for(var i = 0; i < n; i++) {
        r[i] = s;
    }
    return r.join('');
}

function pad (source, c, n) {
    var diff = n - source.length;
    return ns(c, diff) + source;
}

function range (n, fn) {
    var ret = [];
    fn = fn || function (a) {return a;};
    for (var i = 0; i < n; i++) {
        ret.push(fn(i));
    }
    return ret;
}

module.exports.STOMP = {
    'sock': process.env.STOMP_SOCK || 'http://10.102.38.118:8080/qpevents/events',
    'sub': process.env.STOMP_SUB || '/topic/event/**',
    'headers': {}
};


module.exports.COUNTERS = {
    '0': range(24, function(i){return 'Guichet_0_' + pad(i.toString(), '0', 2);}),
    '1': range(17, function(i){return 'Guichet_1_' + pad(i.toString(), '0', 2);})
};

module.exports.COUNTER_NAME_SEP = process.env.COUNTER_NAME_SEP || '_';
module.exports.STORY = process.env.STORY || '0';
module.exports.SLOTS = process.env.SLOTS || 4;

module.exports.SOUND_URL = process.env.SOUND_URL || 'bip.wav';

module.exports.EVENT_GEN = process.env.EVENT_GEN || true;
