
var path = require('path'),
    webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    autoprefixer = require('autoprefixer');





var sourceDir = path.resolve(__dirname, 'src'),
    destDir = process.env.MO_BUILD || path.resolve(__dirname, 'build');

function source (p) {
    return path.join(sourceDir, p);
}

function dest (p) {
    return path.join(destDir, p);
}

var loaders = [],
    plugins =[];


// strict loader
// loaders.push({
//     'test': /\.js$/,
//     'loader': 'strict'
// });

plugins.push(new HtmlWebpackPlugin({
    'title': 'Molenbeek Line Display',
    'chunks': ['main'],
    'template': './index.ejs'
}));

// plugins.push(new webpack.optimize.CommonsChunkPlugin('main.js'));

var webpackOptions = {
    context: source(''),
    entry: {
        'main': './main',
        'service': './service'
    },
    output: {
        path: destDir,
        // publicPath: '/',
        filename: '[name].js'
    },
    module: {
        loaders: loaders
    },
    plugins: plugins,

    resolve: { fallback: path.join(__dirname, 'node_modules') },
    resolveLoader: { fallback: path.join(__dirname, 'node_modules') }
};

webpackOptions.devtool = 'source-map';

module.exports = webpackOptions;
